FROM openjdk:11

ARG PROFILE
ARG ADDITIONAL_OPTS

ENV PROFILE=${PROFILE}
ENV ADDITIONAL_OPTS=${ADDITIONAL_OPTS}

WORKDIR /opt/jhones_petshop

COPY /target/Jhones-PetShop-DevProjetoAplicado-IFTM-0.0.1-SNAPSHOT.jar jhones_petshop_devprojetoaplicado_iftm.jar

SHELL ["/bin/sh", "-c"]

EXPOSE 5005
EXPOSE 8080

CMD java ${ADDITIONAL_OPTS} -jar jhones_petshop_devprojetoaplicado_iftm.jar --spring.profiles.active=${PROFILE}