package com.example.JhonesPetShopDevProjetoAplicadoIFTM;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "com.example.JhonesPetShopDevProjetoAplicadoIFTM")
@EntityScan("com.example.JhonesPetShopDevProjetoAplicadoIFTM.model")
public class JhonesPetShopDevProjetoAplicadoIftmApplication {

	public static void main(String[] args) {
		SpringApplication.run(JhonesPetShopDevProjetoAplicadoIftmApplication.class, args);
	}

}
