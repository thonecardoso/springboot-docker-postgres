package com.example.JhonesPetShopDevProjetoAplicadoIFTM.controller;

import com.example.JhonesPetShopDevProjetoAplicadoIFTM.Repository.ClienteRepository;
import com.example.JhonesPetShopDevProjetoAplicadoIFTM.controller.dto.FisicaRs;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    private final ClienteRepository clienteRepository;

    public ClienteController(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @GetMapping("/")
    public List<FisicaRs> findAll(){

        System.out.println("estou aqui");
        var fisica = clienteRepository.findAll();
        return fisica
                .stream()
                .map(FisicaRs::converter)
                .collect(Collectors.toList());
    }
}
