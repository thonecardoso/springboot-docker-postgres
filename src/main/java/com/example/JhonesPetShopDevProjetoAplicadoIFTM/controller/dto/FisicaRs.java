package com.example.JhonesPetShopDevProjetoAplicadoIFTM.controller.dto;

import com.example.JhonesPetShopDevProjetoAplicadoIFTM.model.Fisica;
import lombok.Data;

@Data
public class FisicaRs extends ClienteRs{

    private String cpf;

    public static FisicaRs converter(Fisica f){
        var fisica = new FisicaRs();

        fisica.setId(f.getId());
        fisica.setNome(f.getNome());
        fisica.setLogradouro(f.getLogradouro());
        fisica.setNumero(f.getNumero());
        fisica.setComplemento(f.getComplemento());
        fisica.setBairro(f.getBairro());
        fisica.setMunicipio(f.getMunicipio());
        fisica.setEstado(f.getEstado());
        fisica.setTelefone(f.getTelefone());
        fisica.setCpf(f.getCpf());

        return fisica;

    }
}
