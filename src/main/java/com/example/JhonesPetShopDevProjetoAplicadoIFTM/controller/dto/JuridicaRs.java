package com.example.JhonesPetShopDevProjetoAplicadoIFTM.controller.dto;

import com.example.JhonesPetShopDevProjetoAplicadoIFTM.model.Juridica;
import lombok.Data;

@Data
public class JuridicaRs extends ClienteRs{

    private String cnpj;

    public static JuridicaRs converter(Juridica j){
        var juridica = new JuridicaRs();

        juridica.setId(j.getId());
        juridica.setNome(j.getNome());
        juridica.setLogradouro(j.getLogradouro());
        juridica.setNumero(j.getNumero());
        juridica.setComplemento(j.getComplemento());
        juridica.setBairro(j.getBairro());
        juridica.setMunicipio(j.getMunicipio());
        juridica.setEstado(j.getEstado());
        juridica.setTelefone(j.getTelefone());
        juridica.setCnpj(j.getCnpj());

        return juridica;

    }
}
