package com.example.JhonesPetShopDevProjetoAplicadoIFTM.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@ToString
@Entity
@Table(name = "fisica")
public class Fisica extends Cliente{

    @Column(name = "cpf")
    private String cpf;

}
