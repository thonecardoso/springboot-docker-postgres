package com.example.JhonesPetShopDevProjetoAplicadoIFTM.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@ToString
@Entity
@Table(name = "juridica")
public class Juridica extends Cliente{

    @Column(name= "cnpj")
    public String cnpj;


}
